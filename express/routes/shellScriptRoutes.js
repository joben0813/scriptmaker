// express-app/routes/shellScriptRoutes.js
const express = require('express');
const router = express.Router();
const shellScriptController = require('../controllers/shellScriptController');

router.post('/createShellScript', (req, res) => shellScriptController.createShellScript(req, res, req.activeSocket, req.childProcess, req.activeAnalyzeSocket));
router.post('/updateShellScript', (req, res) => shellScriptController.updateShellScript(req, res, req.activeSocket, req.childProcess, req.activeAnalyzeSocket));
router.post('/fixShellScript', (req, res) => shellScriptController.fixShellScript(req, res, req.activeSocket, req.childProcess, req.activeAnalyzeSocket));
router.post('/runShellScript', (req, res) => shellScriptController.runShellScript(req, res, req.activeSocket, req.childProcess, req.activeAnalyzeSocket));
router.post('/killShellScript', (req, res) => shellScriptController.killShellScript(req, res, req.activeSocket, req.childProcess, req.activeAnalyzeSocket));
router.get('/getShellScript', (req, res) => shellScriptController.getShellScript(req, res, req.activeSocket, req.childProcess, req.activeAnalyzeSocket));
router.post('/saveShellScript', (req, res) => shellScriptController.saveShellScript(req, res, req.activeSocket, req.childProcess, req.activeAnalyzeSocket));
router.post('/analyzeShellScript', (req, res) => shellScriptController.analyzeShellScript(req, res, req.activeSocket, req.childProcess, req.activeAnalyzeSocket));

module.exports = router;
