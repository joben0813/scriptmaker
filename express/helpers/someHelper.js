const { exec, spawn } = require('child_process');
const stringSimilarity = require('string-similarity');
const fs = require('fs');
const axios = require('axios');

let storedStdout = '';
let storedStderr = '';
let totalTokensUsed = 0;

const ROLE_CODE_GENERATOR = 'code_generator';
const MAX_TOKENS = 3500; // Adjust based on your subscription's token limit

const sanitizeShellScript = (scriptContent) => {
    const lines = scriptContent.split('\n');
    const filteredLines = lines.filter((line) => line.trim() !== 'bash');
    return filteredLines.join('\n');
};



const runChildProcess = (scriptPath, childProcess, activeSocket) => {
    const child = spawn('expect', ['./expect_script.exp', scriptPath], { stdio: [null, 'pipe', 'pipe'] });
    childProcess = child;

    ['stdout', 'stderr'].forEach(stream => child[stream].on('data', (data) => {

        if (stream === 'stdout') {
            storedStdout += data;
        } else {
            storedStderr += data;
        }

        console.log(`${stream}: ${data}`);
        activeSocket.send(`${stream}: ${data}`);
    }));

    child.on('error', (error) => {
        console.error(`error: ${error}`);
        activeSocket.send(`error: ${error}`);
    });

    child.on('close', (code) => {
        if (code === 0) {
            console.log('Shell script finished successfully.');
            activeSocket.send('Shell script finished successfully.');
        } else {
            console.error(`child process exited with code ${code}`);
            activeSocket.send(`child process exited with code ${code}`);
        }
    });
};

const writeToShellScript = (sanitizedCode, activeSocket) => {
    fs.writeFileSync('shell_script.sh', sanitizedCode);
    console.log('New shell script created.');
    activeSocket.send('Shell script created successfully.');
  };
  
  const generateCode = async (prompt, previousCode, operation = 'create', activeSocket) => {
    activeSocket.send('Generating code for your shell script using ChatGPT');
    activeSocket.send('----------------------------------------------');
  
    let initialPrompt;
  
    switch (operation) {
      case 'create':
        initialPrompt = `${'#!/bin/bash\n\nCreate a project, install necessary dependencies, and add code to the required files.'}\n\n- Code snippet: ${prompt}\n\n${ROLE_CODE_GENERATOR}: `;
        break;
      case 'update':
        initialPrompt = `${previousCode}\n\nContinue building the project in the shell script. Any updates to existing code should replace or insert code where it makes sense.:\n- Code snippet: ${prompt}\n\n${ROLE_CODE_GENERATOR}: `;
        break;
      case 'fix':
        initialPrompt = `${previousCode}\n\nFix the issues in the shell script and add the necessary code:\n- Code snippet: ${prompt}\n\n${ROLE_CODE_GENERATOR}: `;
        break;
      default:
        throw new Error('Invalid operation specified');
    }
  
    let port_message = "Any process this shell script creates should be run on port 3006. This is extremely important!";
  
    const initialPromptChunks = splitTextIntoChunks(initialPrompt, MAX_TOKENS / 2);
  
    let codeSnippet = '';
  
    for (const initialPromptChunk of initialPromptChunks) {
  
    let messages = [
      { role: 'system', content: `${ROLE_CODE_GENERATOR}:` },
      { role: 'user', content: initialPromptChunk },
      { role: 'system', content: port_message },
      { role: 'user', content: port_message },
    ];
  
    let broken_length = 0;
  
    if (initialPromptChunks.length > 1) {
      let instruct = 'This script exceeds the maximum number of tokens allowed by the API. So this is only a portion. The remainder will come in subsequent responses.'
      messages.push({ role: 'system', content: instruct });
      broken_length = instruct.length;
    }
      const tokensLeft = MAX_TOKENS - initialPromptChunk.length - (port_message.length * 2) - ROLE_CODE_GENERATOR.length - broken_length;
  
      const chatGPTResponse = await axios.post(
        'https://api.openai.com/v1/chat/completions',
        {
          model: 'gpt-3.5-turbo',
          max_tokens: tokensLeft,
          messages: messages,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${process.env.CHATGPT_API_KEY}`,
          },
        }
      );
  
      const currentCodeSnippet = extractCodeSnippet(chatGPTResponse.data.choices[0].message.content);
      codeSnippet += currentCodeSnippet;
      activeSocket.send(currentCodeSnippet);
      activeSocket.send('----------------------------------------------');
    }
  
    const consolidatedCode = consolidateCode(previousCode, codeSnippet);
    const hasMore = checkIfMore(prompt, codeSnippet);
  
    return hasMore ? await generateCode(prompt, consolidatedCode, operation) : consolidatedCode;
  };
  
  const consolidateCode = (previousCode, codeSnippet) => {
    const existingLines = previousCode.split('\n');
    const newLines = codeSnippet.split('\n');
    const consolidatedLines = [];
  
    newLines.forEach((line) => {
      if (!existingLines.includes(line)) {
        consolidatedLines.push(line);
      }
    });
  
    return previousCode + '\n' + consolidatedLines.join('\n');
  };
  
  const checkIfMore = (prompt, codeSnippet) => {
    return codeSnippet.toLowerCase().includes(prompt.toLowerCase());
  };
  
  const extractCodeSnippet = (responseText) => {
    const matched = responseText.match(/```[\s\S]*?```/g);
    if (matched && matched.length > 0) {
      return matched[0].replace(/```/g, '').trim();
    }
    return '';
  };

  const filterSimilarOutputs = (outputs, similarityThreshold = 0.8) => {
    const filteredOutputs = [];
  
    for (const output of outputs) {
      let isSimilar = false;
  
      for (const filteredOutput of filteredOutputs) {
        if (stringSimilarity.compareTwoStrings(output, filteredOutput) > similarityThreshold) {
          isSimilar = true;
          break;
        }
      }
  
      if (!isSimilar) {
        filteredOutputs.push(output);
      }
    }
  
    return filteredOutputs;
  };

  const generateCodeWithOutput = async (shellScript, activeSocket) => {
    // Combine stdout and stderr
    const combinedOutput = `${storedStdout}\n${storedStderr}`;
  
    // Split the outputs into lines and filter out similar outputs
    const outputLines = combinedOutput.split('\n');
    const uniqueOutputLines = filterSimilarOutputs(outputLines);
  
    // Join the filtered output lines and split into chunks
    const filteredOutput = uniqueOutputLines.join('\n');
    const outputChunks = splitTextIntoChunks(filteredOutput, MAX_TOKENS / 2);
  
    // Split the shell script input into chunks
    const scriptChunks = splitTextIntoChunks(shellScript, MAX_TOKENS / 2);
  
    let result = '';
  
    let instruction = 'Analyze the shell script, which may be incomplete(remainder will come in subsequent requests), and the output from running the shell script, and provide any updates/fixes to the shell script.';
  
    // const tokensLeft = MAX_TOKENS - initialPrompt.length - (port_message.length * 2) - ROLE_CODE_GENERATOR.length;
  
    // Iterate through each pair of shell script and output chunks
    // for (const scriptChunk of scriptChunks) {
      for (const outputChunk of outputChunks) {
        const chatGPTResponse = await axios.post(
          'https://api.openai.com/v1/chat/completions',
          {
            model: 'gpt-3.5-turbo',
            max_tokens: MAX_TOKENS,
            messages: [
              { role: 'system', content: `${ROLE_CODE_GENERATOR}:` },
              { role: 'system', content: instruction },
              { role: 'user', content: `Shell Script:\n${shellScript}\n\nOutput:\n${outputChunk}` },
            ],
          },
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${process.env.CHATGPT_API_KEY}`,
            },
          }
        );
  
        result += chatGPTResponse.data.choices[0].message.content;
        activeSocket.send(chatGPTResponse.data.choices[0].message.content);
        // Add the token count from the response to the totalTokensUsed variable
        totalTokensUsed += chatGPTResponse.data.usage.total_tokens;
        console.log(`totalTokensUsed: ${totalTokensUsed}}`);
      }
    // }
  
    return result;
  };
  
  
  const splitTextIntoChunks = (text, chunkSize) => {
    const regex = new RegExp(`.{1,${chunkSize}}`, 'g');
    return text.match(regex) || [];
  };

  module.exports = {
    sanitizeShellScript,
    runChildProcess,
    writeToShellScript,
    generateCode,
    consolidateCode,
    checkIfMore,
    extractCodeSnippet,
    filterSimilarOutputs,
    generateCodeWithOutput,
    splitTextIntoChunks,
  };