const express = require('express');
const axios = require('axios');
const fs = require('fs');
const cors = require('cors');
const WebSocket = require('ws');




const app = express();
const wss = new WebSocket.Server({ port: 3005 });
const wssAnalyze = new WebSocket.Server({ port: 3007 });




app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


let childProcess;
let activeSocket = null;
let activeAnalyzeSocket = null;

wss.on('connection', (socket) => {
    console.log('WebSocket connected.');
    activeSocket = socket;
  
    socket.on('message', async (message) => {
      if (message === 'request_data') {
        try {
          const data = fs.readFileSync('shell_script.sh', 'utf-8');
          socket.send(`Current shell script contents:\n\n${data}`);
        } catch (error) {
          console.error('Error reading shell_script.sh:', error);
          socket.send('Error reading shell_script.sh.');
        }
      }
    });
  
    socket.on('close', () => {
      console.log('WebSocket disconnected.');
    });
  });

wssAnalyze.on('connection', (socket) => {
  console.log('WebSocket for analyzeShellScript connected.');
  activeAnalyzeSocket = socket;

  socket.on('close', () => {
    console.log('WebSocket for analyzeShellScript disconnected.');
  });
});

const socketMiddleware = (req, res, next) => {
    req.activeSocket = activeSocket;
    req.activeAnalyzeSocket = activeAnalyzeSocket;
    req.childProcess = childProcess;
    next();
  };
  

const shellScriptRoutes = require('./routes/shellScriptRoutes');
app.use('/', socketMiddleware, shellScriptRoutes);

app.listen(3003, () => console.log('Server listening on port 3003'));