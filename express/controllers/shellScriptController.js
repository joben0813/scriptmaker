// express-app/controllers/shellScriptController.js
const fs = require('fs');
// const { generateCodeWithOutput } = require('../helpers/someHelper');
const helpers = require('../helpers/someHelper');



exports.createShellScript = async (req, res, activeSocket) => {
    const { prompt } = req.body;

    try {
        const code = await helpers.generateCode(prompt, '', 'create', activeSocket);
        const sanitizedCode = helpers.sanitizeShellScript(code);
        helpers.writeToShellScript(sanitizedCode, activeSocket);
        res.status(200).send('Shell script created successfully.');
    } catch (error) {
        console.error(error);
        res.status(500).send('Failed to create shell script.');
    }
};

exports.updateShellScript = async (req, res) => {
    const { prompt } = req.body;

    try {
        const previousCode = fs.readFileSync('shell_script.sh', 'utf-8');
        const code = await helpers.generateCode(prompt, previousCode, 'update', activeSocket);
        const sanitizedCode = helpers.sanitizeShellScript(code);
        helpers.writeToShellScript(sanitizedCode, activeSocket);
        res.status(200).send('Shell script updated successfully.');
    } catch (error) {
        console.error(error);
        res.status(500).send('Failed to update shell script.');
    }
};

exports.fixShellScript = async (req, res) => {
    const { prompt } = req.body;

  try {
    const previousCode = fs.readFileSync('shell_script.sh', 'utf-8');
    const code = await helpers.generateCode(prompt, previousCode, 'fix', activeSocket);
    const sanitizedCode = helpers.sanitizeShellScript(code);
    helpers.writeToShellScript(sanitizedCode, activeSocket);
    res.status(200).send('Shell script fixed successfully.');
  } catch (error) {
    console.error(error);
    res.status(500).send('Failed to fix shell script.');
  }
};

exports.runShellScript = async (req, res, activeSocket, childProcess) => {
    if (activeSocket) {
        await helpers.runChildProcess('shell_script.sh', childProcess, activeSocket);
        console.log('Shell script executed successfully.');
        activeSocket.send('Shell script executed successfully.');
        res.status(200).send('Shell script executed successfully.');
    } else {
        res.status(500).send('No active WebSocket connection.');
    }
};

exports.killShellScript = async (req, res, activeSocket, childProcess) => {
    if (childProcess) {
        exec("lsof -i :3006 -t | xargs kill -9", (error, stdout, stderr) => {
            if (error) {
                console.error(`Error killing process on port 3006: ${error}`);
            } else {
                console.log("Successfully killed process on port 3006");
            }
        });
        childProcess.kill('SIGINT');
        activeSocket.send('Shell script killed.');
    }
    res.sendStatus(200);
};

exports.getShellScript = async (req, res) => {
  fs.readFile("shell_script.sh", 'utf-8', (err, data) => {
    if (err) {
      console.error(err);
      res.status(500).send('Error reading shell script');
    } else {
      res.send(data);
    }
  });
};

exports.saveShellScript = async (req, res) => {
    const { scriptContent } = req.body;

  fs.writeFile("shell_script.sh", scriptContent, 'utf-8', (err) => {
    if (err) {
      console.error(err);
      res.status(500).send('Error saving shell script');
    } else {
      res.status(200).send('Shell script saved successfully');
    }
  });
};

exports.analyzeShellScript = async (req, res, activeSocket, childProcess, activeAnalyzeSocket) => {
    if (!activeAnalyzeSocket) {
        res.status(500).send('No active WebSocket connection for analyzeShellScript.');
        return;
      }
    
      try {
        const shellScript = fs.readFileSync('shell_script.sh', 'utf-8');
        const suggestions = await helpers.generateCodeWithOutput(shellScript, activeSocket);
    
        // Stream suggestions in smaller packets
        const suggestionChunks = helpers.splitTextIntoChunks(suggestions, MAX_TOKENS / 2);
        suggestionChunks.forEach(chunk => {
          activeAnalyzeSocket.send(chunk);
        });
    
        res.status(200).send('Suggestions sent successfully.');
      } catch (error) {
        console.error(error);
        res.status(500).send('Failed to analyze shell script and output.');
      }
};
