# ScriptMaker

- Clone the repository
- open 2 terminal sessions, one for `express` and one for `nextjs`
- run `npm i` in both
- add your API KEY to the respective field in the `Dockerfile` in the `express` app
- run `npm run dev` in your `nextjs` terminal session
- in your express app, run `docker build -t my-node-app .` followed by `docker run -it -p 3003:3003 -p 3005:3005 -p 3006:3006 -p 3007:3007 --name my-node-container my-node-app`
- visit `localhost:3000`, enter a prompt for the script, and click `Create Shell Script`
