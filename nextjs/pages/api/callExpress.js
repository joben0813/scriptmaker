app.post('/createShellScript', createShellScript);

const updateShellScript = async (req, res) => {
  const { prompt } = req.body;

  try {
    const previousCode = fs.readFileSync('shell_script.sh', 'utf-8');
    const code = await generateCode(prompt, previousCode);
    fs.writeFileSync('shell_script.sh', code);
    res.status(200).send('Shell script updated successfully.');
  } catch (error) {
    console.error(error);
    res.status(500).send('Failed to update shell script.');
  }
};

app.post('/updateShellScript', updateShellScript);