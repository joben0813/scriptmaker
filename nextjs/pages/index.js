import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

const TerminalBox = ({ output, onPromptChange, promptValue }) => {
  const containerRef = useRef(null);

  useEffect(() => {
    if (containerRef.current) {
      containerRef.current.scrollTop = containerRef.current.scrollHeight;
    }
  }, [output]);

  return (
    <Box
      component="pre"
      sx={{
        backgroundColor: 'black',
        color: 'lime',
        padding: 2,
        borderRadius: 1,
        fontFamily: 'monospace',
        fontSize: '0.9em',
        whiteSpace: 'pre-wrap',
        wordBreak: 'break-all',
        overflowWrap: 'break-word',
        maxHeight: '400px',
        overflowY: 'auto',
      }}
      className="terminal-box"
      ref={containerRef}
    >
      {output &&
        output.map((line, index) => (
          <Typography key={index} component="span">
            {line}
            {'\n'}
          </Typography>
        ))}
      <TextField
        InputProps={{
          disableUnderline: true,
          sx: { color: 'lime', fontFamily: 'monospace' },
        }}
        value={promptValue}
        onChange={onPromptChange}
        placeholder="Enter a code prompt"
        fullWidth
      />
    </Box>
  );
};

const IndexPage = () => {
  const [output, setOutput] = useState([]);
  const [socket, setSocket] = useState(null);
  const [prompt, setPrompt] = useState('');
  const [update, setUpdate] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [scriptContent, setScriptContent] = useState('');
  const [analysis, setAnalysis] = useState('');
  const [activeTab, setActiveTab] = useState(0);

  const handleTabChange = (event, newValue) => {
    setActiveTab(newValue);
  };

    const clearTerminal = () => {
    setOutput([]);
  };


  // New state for handling the analysis output
  const [analysisOutput, setAnalysisOutput] = useState([]);

  // New state for the WebSocket connection for the analysis stream
  const [analysisSocket, setAnalysisSocket] = useState(null);

  // Initialize WebSocket connection for the analysis stream
  useEffect(() => {
    const ws = new WebSocket('ws://localhost:3007');

    ws.onopen = () => {
      console.log('WebSocket connection for analysis opened.');
    };

    ws.onerror = (error) => {
      console.error(`WebSocket error for analysis: ${error}`);
    };

    ws.onclose = () => {
      console.log('WebSocket connection for analysis closed.');
    };

    setAnalysisSocket(ws);

    return () => {
      ws.close();
    };
  }, []);

  // Set up WebSocket message listener for the analysis stream
  useEffect(() => {
    if (analysisSocket) {
      analysisSocket.onmessage = (event) => {
        const message = event.data;
        setAnalysisOutput((prevOutput) => [...prevOutput, message]);
      };
    }
  }, [analysisSocket]);

  // Initialize WebSocket connection
  useEffect(() => {
    const ws = new WebSocket('ws://localhost:3005');

    ws.onopen = () => {
      console.log('WebSocket connection opened.');
    };

    ws.onerror = (error) => {
      console.error(`WebSocket error: ${error}`);
    };

    ws.onclose = () => {
      console.log('WebSocket connection closed.');
    };

    setSocket(ws);

    return () => {
      ws.close();
    };
  }, []);

  // Set up WebSocket message listener
  useEffect(() => {
    if (socket) {
      socket.onmessage = (event) => {
        const message = event.data;
        setOutput((prevOutput) => [...prevOutput, message]);
      };
    }
  }, [socket]);

      const callApi = async (event) => {
      event.preventDefault();
      setLoading(true);
      setError(null);

      // Wait for the WebSocket to be in the OPEN state
      const waitForSocketOpen = () => {
        return new Promise((resolve) => {
          const checkSocketOpen = () => {
            if (socket.readyState === WebSocket.OPEN) {
              resolve();
            } else {
              setTimeout(checkSocketOpen, 100);
            }
          };
          checkSocketOpen();
        });
      };

      try {
        await waitForSocketOpen();
        const response = await axios.post(
          update
            ? 'http://localhost:3003/updateShellScript'
            : 'http://localhost:3003/createShellScript',
          { prompt }
        );
      } catch (error) {
        setError('Failed to call Express server.');
      } finally {
        setLoading(false);
      }
    };

    const [shellScript, setShellScript] = useState('');
const [scriptLoading, setScriptLoading] = useState(false);
const [scriptError, setScriptError] = useState(null);

const fetchShellScript = async () => {
  setScriptLoading(true);
  setScriptError(null);
  try {
    const response = await axios.get('http://localhost:3003/getShellScript');
    setShellScript(response.data);
  } catch (error) {
    setScriptError('Failed to fetch the shell script.');
  } finally {
    setScriptLoading(false);
  }
};

const saveShellScript = async () => {
  try {
    await axios.post('http://localhost:3003/saveShellScript', { scriptContent: shellScript });
    alert('Shell script saved successfully.');
  } catch (error) {
    alert('Failed to save the shell script.');
  }
};

  const fixShellScript = async () => {
    const response = await axios.post('http://localhost:3003/fixShellScript', { prompt });
  }
  const runShellScript = async () => {
    const response = await axios.post('http://localhost:3003/runShellScript', { prompt });
  }
  const killShellScript = async () => {
    const response = await axios.post('http://localhost:3003/killShellScript', { prompt });
  }

  // Set up WebSocket message listener
  useEffect(() => {
    if (socket) {
      socket.onmessage = (event) => {
        const message = event.data;
        setOutput((prevOutput) => [...prevOutput, message]);
      };
    }
  }, [socket]);

  const [iframeKey, setIframeKey] = useState(0);

  const reloadIframe = async () => {
    try {
      const response = await fetch("http://localhost:3006", { mode: "no-cors" });
      const frame = document.getElementById("iframe");
      frame.src = "http://localhost:3006";
    } catch (error) {
      console.log("Content at localhost:3006 is not available yet");
    }
  };
  
  const analyzeShellScript = async () => {
    try {
      const response = await axios.post('http://localhost:3003/analyzeShellScript', {
        shellScript,
        stdout: output.join('\n'), // Assuming 'output' is the array of stdout lines
        stderr: '', // Add a way to capture stderr in your Next.js app and pass it here
      });
  
      setAnalysis(response.data);
    } catch (error) {
      setError('Failed to analyze the shell script.');
    }
  };
  

  return (
    <div>
      <h1>Next.js App</h1>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <h1>Tabbed View</h1>
          <Paper elevation={3} style={{ padding: '16px' }}>
            <Tabs value={activeTab} onChange={handleTabChange}>
              <Tab label="Terminal" />
              <Tab label="Edit Script" />
              <Tab label="Analyze Output" />
            </Tabs>
            {activeTab === 0 && (
              <>
                <Grid item xs={12} sm={6}>
        <h1>Terminal-like Display Example</h1>
        <Paper elevation={3} style={{ padding: '16px' }}>
          <TerminalBox output={output} />
        </Paper>
        <form onSubmit={callApi}>
        <TextField
        label="Code Prompt"
        variant="outlined"
        value={prompt}
        onChange={(e) => setPrompt(e.target.value)}
        style={{ marginRight: '10px' }}
      />
      <label>
        <input
          type="checkbox"
          checked={update}
          onChange={() => setUpdate(!update)}
        />
        Update existing script
      </label>
      <Button variant="contained" color="primary" type="submit">
        Generate Shell Script
      </Button>
    </form>
    <Button variant="outlined" color="secondary" onClick={fixShellScript}>
      Fix Shell Script
    </Button>
    <Button variant="outlined" color="secondary" onClick={runShellScript}>
      Run Shell Script
    </Button>
    <Button variant="outlined" color="secondary" onClick={killShellScript}>
      Kill Shell Script
    </Button>
    <Button variant="outlined" color="secondary" onClick={clearTerminal}>
      Clear Terminal
    </Button>

      </Grid>
              </>
            )}
            {activeTab === 1 && (
              <>
                <Grid item xs={12} sm={6}>
        <h1>Edit Shell Script</h1>
        <Button variant="outlined" color="primary" onClick={fetchShellScript}>
          Fetch Shell Script
        </Button>
        {scriptLoading && <p>Loading...</p>}
        {scriptError && <p>{scriptError}</p>}
        <TextField
          value={shellScript}
          onChange={(e) => setShellScript(e.target.value)}
          multiline
          rows={16}
          fullWidth
          variant="outlined"
          style={{ marginTop: '10px' }}
        />
        <Button
          variant="contained"
          color="primary"
          onClick={saveShellScript}
          style={{ marginTop: '10px' }}
        >
          Save Shell Script
        </Button>

        <Button variant="outlined" color="secondary" onClick={analyzeShellScript}>
        Analyze Shell Script
        </Button>

        {analysis && (
        <div>
          <h3>Analysis:</h3>
          <pre>{analysis}</pre>
        </div>
        )}
      </Grid>
              </>
            )}
            {activeTab === 2 && (
              <>
                <Grid item xs={12} sm={6}>
                <h2>Analyze Output</h2>
                <TerminalBox
                  output={analysisOutput}
                  onPromptChange={(e) => setPrompt(e.target.value)}
                  promptValue={prompt}
                />
                <Button
                  variant="outlined"
                  color="secondary"
                  onClick={analyzeShellScript}
                >
                  Analyze Shell Script
                </Button>
                </Grid>
              </>
            )}
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
        <h1>Local Process (Port 3006)</h1>
        <Button variant="outlined" color="primary" onClick={reloadIframe}>
          Reload iframe
        </Button>
        <Paper
          elevation={3}
          style={{ width: '100%', height: '400px', marginTop: '16px' }}
        >
          <iframe
            id="iframe"
            src="http://localhost:3006"
            style={{
              width: '100%',
              height: '100%',
              border: 'none',
            }}
            title="Local Process"
          ></iframe>
        </Paper>
      </Grid>
      </Grid>
    </div>
  );
};

export default IndexPage;




// return (
//   <div>
//     <h1>Next.js App</h1>
//     <form onSubmit={callApi}>
//       <TextField
//         label="Code Prompt"
//         variant="outlined"
//         value={prompt}
//         onChange={(e) => setPrompt(e.target.value)}
//         style={{ marginRight: '10px' }}
//       />
//       <label>
//         <input
//           type="checkbox"
//           checked={update}
//           onChange={() => setUpdate(!update)}
//         />
//         Update existing script
//       </label>
//       <Button variant="contained" color="primary" type="submit">
//         Generate Shell Script
//       </Button>
//     </form>
//     <Button variant="outlined" color="secondary" onClick={fixShellScript}>
//       Fix Shell Script
//     </Button>
//     <Button variant="outlined" color="secondary" onClick={runShellScript}>
//       Run Shell Script
//     </Button>
//     <Button variant="outlined" color="secondary" onClick={killShellScript}>
//       Kill Shell Script
//     </Button>
//     <Button variant="outlined" color="secondary" onClick={clearTerminal}>
//       Clear Terminal
//     </Button>

//     {loading && <p>Loading...</p>}
//     {error && <p>{error}</p>}

//     <Grid container spacing={2}>
//       <Grid item xs={12} sm={6}>
//         <h1>Terminal-like Display Example</h1>
//         <Paper elevation={3} style={{ padding: '16px' }}>
//           <TerminalBox output={output} />
//         </Paper>
//       </Grid>
//       <Grid item xs={12} sm={6}>
//         <h1>Local Process (Port 3006)</h1>
//         <Button variant="outlined" color="primary" onClick={reloadIframe}>
//           Reload iframe
//         </Button>
//         <Paper
//           elevation={3}
//           style={{ width: '100%', height: '400px', marginTop: '16px' }}
//         >
//           <iframe
//             id="iframe"
//             src="http://localhost:3006"
//             style={{
//               width: '100%',
//               height: '100%',
//               border: 'none',
//             }}
//             title="Local Process"
//           ></iframe>
//         </Paper>
//       </Grid>
//       <Grid item xs={12} sm={6}>
//         <h1>Edit Shell Script</h1>
//         <Button variant="outlined" color="primary" onClick={fetchShellScript}>
//           Fetch Shell Script
//         </Button>
//         {scriptLoading && <p>Loading...</p>}
//         {scriptError && <p>{scriptError}</p>}
//         <TextField
//           value={shellScript}
//           onChange={(e) => setShellScript(e.target.value)}
//           multiline
//           rows={16}
//           fullWidth
//           variant="outlined"
//           style={{ marginTop: '10px' }}
//         />
//         <Button
//           variant="contained"
//           color="primary"
//           onClick={saveShellScript}
//           style={{ marginTop: '10px' }}
//         >
//           Save Shell Script
//         </Button>

//         <Button variant="outlined" color="secondary" onClick={analyzeShellScript}>
//         Analyze Shell Script
//         </Button>

//         {analysis && (
//         <div>
//           <h3>Analysis:</h3>
//           <pre>{analysis}</pre>
//         </div>
//         )}
//       </Grid>
//     </Grid>
//   </div>
// );


    // The remaining code remains unchanged.

// import React, { useState, useEffect, useRef } from 'react';
// import axios from 'axios';
// // import React from 'react';
// import Box from '@mui/material/Box';
// import Typography from '@mui/material/Typography';


// const TerminalBox = ({ output }) => {
//   const containerRef = useRef(null);

//   useEffect(() => {
//     if (containerRef.current) {
//       containerRef.current.scrollTop = containerRef.current.scrollHeight;
//     }
//   }, [output]);
//   return (
//     <Box
//       component="pre"
//       sx={{
//         backgroundColor: 'black',
//         color: 'lime',
//         padding: 2,
//         borderRadius: 1,
//         fontFamily: 'monospace',
//         fontSize: '0.9em',
//         whiteSpace: 'pre-wrap',
//         wordBreak: 'break-all',
//         overflowWrap: 'break-word',
//         maxHeight: '400px',
//         overflowY: 'auto',
//       }}
//       className="terminal-box" 
//       ref={containerRef}
//     >
//       {output && output.map((line, index) => (
//         <Typography key={index} component="span">
//           {line}
//           {'\n'}
//         </Typography>
//       ))}
//     </Box>
//   );
// };

// const IndexPage = () => {
//   const [output, setOutput] = useState([]);
//   const [socket, setSocket] = useState(null);


//   const clearTerminal = () => {
//     setOutput([]);
//   };

  
//   // Initialize WebSocket connection
//   useEffect(() => {
//     const ws = new WebSocket('ws://localhost:3005');

//     ws.onopen = () => {
//       console.log('WebSocket connection opened.');
//     };

//     ws.onerror = (error) => {
//       console.error(`WebSocket error: ${error}`);
//     };

//     ws.onclose = () => {
//       console.log('WebSocket connection closed.');
//     };

//     setSocket(ws);

//     return () => {
//       ws.close();
//     };
//   }, []);


//   // calling the express server
//     const [prompt, setPrompt] = useState('');
//   const [update, setUpdate] = useState(false);
//   const [loading, setLoading] = useState(false);
//   const [error, setError] = useState(null);

//   const callApi = async (event) => {
//     event.preventDefault();
//     setLoading(true);
//     setError(null);
  
//     // Wait for the WebSocket to be in the OPEN state
//     const waitForSocketOpen = () => {
//       return new Promise((resolve) => {
//         const checkSocketOpen = () => {
//           if (socket.readyState === WebSocket.OPEN) {
//             resolve();
//           } else {
//             setTimeout(checkSocketOpen, 100);
//           }
//         };
//         checkSocketOpen();
//       });
//     };
  
//     try {
//       await waitForSocketOpen();
//       const response = await axios.post(update ? 'http://localhost:3003/updateShellScript' : 'http://localhost:3003/createShellScript', { prompt });
//     } catch (error) {
//       setError('Failed to call Express server.');
//     } finally {
//       setLoading(false);
//     }
//   };
//   const fixShellScript = async () => {
//     const response = await axios.post('http://localhost:3003/fixShellScript', { prompt });
//   }
//   const runShellScript = async () => {
//     const response = await axios.post('http://localhost:3003/runShellScript', { prompt });
//   }
//   const killShellScript = async () => {
//     const response = await axios.post('http://localhost:3003/killShellScript', { prompt });
//   }

//   // Set up WebSocket message listener
//   useEffect(() => {
//     if (socket) {
//       socket.onmessage = (event) => {
//         const message = event.data;
//         setOutput((prevOutput) => [...prevOutput, message]);
//       };
//     }
//   }, [socket]);

//   const [iframeKey, setIframeKey] = useState(0);

//   const reloadIframe = async () => {
//     try {
//       const response = await fetch("http://localhost:3006", { mode: "no-cors" });
//       const frame = document.getElementById("iframe");
//       frame.src = "http://localhost:3006";
//     } catch (error) {
//       console.log("Content at localhost:3006 is not available yet");
//     }
//   };
  


//   return (
//     <div>
//       <h1>Next.js App</h1>
//        <form onSubmit={callApi}>
//          <input
//            type="text"
//            value={prompt}
//            onChange={(e) => setPrompt(e.target.value)}
//            placeholder="Enter a code prompt"
//          />
//          <label>
//            <input
//              type="checkbox"
//              checked={update}
//              onChange={() => setUpdate(!update)}
//            />
//            Update existing script
//          </label>
//          <button type="submit">Generate Shell Script</button>
//        </form>
//       <button onClick={fixShellScript}>Fix Shell Script</button>
//       <button onClick={runShellScript}>Run Shell Script</button>
//       <button onClick={killShellScript}>Kill Shell Script</button>
//       <button onClick={clearTerminal}>Clear Terminal</button>

//        {loading && <p>Loading...</p>}
//        {error && <p>{error}</p>}
//       {/* <div>
//         <h1>Terminal-like Display Example</h1>
//         <TerminalBox output={output} />
//       </div> */}
//       <div style={{ display: 'flex' }}>
//         <div style={{ flex: 1 }}>
//           <h1>Terminal-like Display Example</h1>
//           <TerminalBox output={output} />
//         </div>
//         <div style={{ flex: 1 }}>
//           <h1>Local Process (Port 3006)</h1>
//           <button onClick={reloadIframe}>Reload iframe</button>
//           <iframe
//           id="iframe"
//             src="http://localhost:3006"
//             style={{ width: '100%', height: '400px', border: '1px solid #ccc' }}
//             title="Local Process"
//           ></iframe>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default IndexPage;

// // import React, { useState, useEffect } from 'react';
// // import axios from 'axios';
// // // import React from 'react';
// // import Box from '@mui/material/Box';
// // import Typography from '@mui/material/Typography';

// // const OutputFeed = ({ output }) => {
// //   return (
// //     <div className="output-feed">
// //       <h2>Output Feed</h2>
// //       <pre>{output}</pre>
// //     </div>
// //   );
// // };

// // const TerminalBox = ({ output }) => {
// //   console.log('output', output)
// //   return (
// //     <Box
// //       component="pre"
// //       sx={{
// //         backgroundColor: 'black',
// //         color: 'lime',
// //         padding: 2,
// //         borderRadius: 1,
// //         fontFamily: 'monospace',
// //         fontSize: '0.9em',
// //         whiteSpace: 'pre-wrap',
// //         wordBreak: 'break-all',
// //         overflowWrap: 'break-word',
// //         maxHeight: '400px',
// //         overflowY: 'auto',
// //       }}
// //     >
// //       {output && output.map((line, index) => (
// //         <Typography key={index} component="span">
// //           {line}
// //           {'\n'}
// //         </Typography>
// //       ))}
// //     </Box>
// //   );
// // };

// // const Index = () => {

// //   const [output, setOutput] = useState('');
// //   const [socket, setSocket] = useState(null);

// //   useEffect(() => {
// //     const socket = new WebSocket('ws://localhost:3005');
// //     setSocket(socket);
  
// //     socket.onmessage = (event) => {
// //       console.log('ever?');
// //       setOutput((prevOutput) => prevOutput + '\n' + event.data);
// //     };
  
// //     return () => {
// //       socket.close();
// //     };
// //   }, []);

// //   useEffect(() => {
// //     const interval = setInterval(() => {
// //       setOutput((prevOutput) => [...prevOutput, '> Example output...']);
// //     }, 1000);

// //     return () => {
// //       clearInterval(interval);
// //     };
// //   }, []);
  


// //   const [prompt, setPrompt] = useState('');
// //   const [update, setUpdate] = useState(false);
// //   const [loading, setLoading] = useState(false);
// //   const [error, setError] = useState(null);

// //   const callApi = async (event) => {
// //     event.preventDefault();
// //     setLoading(true);
// //     setError(null);
  
// //     // Wait for the WebSocket to be in the OPEN state
// //     const waitForSocketOpen = () => {
// //       return new Promise((resolve) => {
// //         const checkSocketOpen = () => {
// //           if (socket.readyState === WebSocket.OPEN) {
// //             resolve();
// //           } else {
// //             setTimeout(checkSocketOpen, 100);
// //           }
// //         };
// //         checkSocketOpen();
// //       });
// //     };
  
// //     try {
// //       await waitForSocketOpen();
// //       const response = await axios.post(update ? 'http://localhost:3003/updateShellScript' : 'http://localhost:3003/createShellScript', { prompt });
// //     } catch (error) {
// //       setError('Failed to call Express server.');
// //     } finally {
// //       setLoading(false);
// //     }
// //   };
  
// //   return (
// //     <div>
// //       <h1>Next.js App</h1>
// //       <form onSubmit={callApi}>
// //         <input
// //           type="text"
// //           value={prompt}
// //           onChange={(e) => setPrompt(e.target.value)}
// //           placeholder="Enter a code prompt"
// //         />
// //         <label>
// //           <input
// //             type="checkbox"
// //             checked={update}
// //             onChange={() => setUpdate(!update)}
// //           />
// //           Update existing script
// //         </label>
// //         <button type="submit">Generate Shell Script</button>
// //       </form>

// //       {loading && <p>Loading...</p>}
// //       {error && <p>{error}</p>}
// //       <div>
// //       <h1>Terminal-like Display Example</h1>
// //       <TerminalBox output={output} />
// //     </div>
// //     </div>
// //   );
// // };

// // export default Index;
